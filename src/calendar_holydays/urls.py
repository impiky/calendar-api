from django.conf.urls import url
from django.urls import path

from calendar_holydays.views import *

app_name = 'calendar_holydays'

urlpatterns = [
    path('updateholydays/', HolydaysAdminView.as_view()),
    path('getcountrys/', CountryListView.as_view()),
    path('getholydays/', HolydayView.as_view()),
]
