from rest_framework import serializers

from .models import CountryList, Holydays


class CountrysListSerializer(serializers.ModelSerializer):

    class Meta:
        model = CountryList
        fields = (
            'country',)


class HolydaysSerializer(serializers.ModelSerializer):
    class Meta:
        model = Holydays
        fields = (
            'country',
            'event_name',
            'begin',
        )