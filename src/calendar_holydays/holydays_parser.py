import re

import requests
from arrow.parser import ParserError
from bs4 import BeautifulSoup
from ics import Calendar
import lxml


def parse_countrys():
    html = requests.get('https://www.officeholidays.com/countries/index.php')
    soup = BeautifulSoup(html.text, 'lxml')
    tds = soup.find('table', attrs={'class' : 'info-table'}).find_all('td', width="33%")
    countrys = []
    for td in tds:
        a = str(td.find('a').get('href'))
        a = re.sub('/index.php', '', a)
        a = re.sub('/', '', a)
        countrys.append(a)
    return countrys


def get_holydays(country_name):
    url = 'https://www.officeholidays.com/ics/ics_country.php?tbl_country={}'.format(country_name)
    holydays = []
    try:
        c = Calendar(requests.get(url).text)
    except ParserError:
        pass
    else:
        for i in c.events:
            holydays.append([i.name, i._begin.datetime])
    return holydays