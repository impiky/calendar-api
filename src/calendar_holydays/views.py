from django.db.utils import IntegrityError
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from .holydays_parser import parse_countrys, get_holydays
from .models import *
from .serializers import *
from calendar_core.models import UserCountry
from datetime import datetime
from django.utils import timezone


class CountryListView(APIView):
    def get(self, request):
        countrys = CountryList.objects.all()
        serializer = CountrysListSerializer(countrys, many=True)
        return Response({'data': serializer.data})


class HolydaysAdminView(APIView):
    permission_classes = [permissions.IsAdminUser]

    def save_holyday(self, country):

        for item in get_holydays(country):
            holyday = Holydays()
            holyday.country = country
            holyday.event_name = item[0]
            holyday.begin = item[1]
            holyday.save()

    def get(self, request):
        CountryList.objects.all().delete()
        Holydays.objects.all().delete()
        countrys_list = parse_countrys()
        for country in range(len(countrys_list)):
            countrylist = CountryList()
            countrylist.country = countrys_list[country]
            try:
                countrylist.save()
            except IntegrityError:
                self.save_holyday(countrys_list[country])
            else:
                self.save_holyday(countrys_list[country])
        if countrylist:
            return Response({'status': "Done"})
        return Response({'status': "Error"})


class HolydayView(APIView):
    def get(self, request):
        if request.user.usercountry.country:
            holydays = Holydays.objects.filter(country=str(request.user.usercountry.country).casefold(), 
                                               begin__month=datetime.today().month,
                                               begin__year=datetime.today().year)
            serializer = HolydaysSerializer(holydays, many=True)
            return Response({'data': serializer.data})
        return Response({'status': 'Error - user has no country'})
        
