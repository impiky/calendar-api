from django.apps import AppConfig


class CalendarHolydaysConfig(AppConfig):
    name = 'calendar_holydays'
