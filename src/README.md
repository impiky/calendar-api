Caledar API based on django rest framework

1. install requirements.txt (pip install -U -r requirements.txt)
2. run django developer server(python manage.py runserver)
3. run redis(redis-server)
4. run Celery (celery run celery worker -A calendar_core --loglevel=debug --concurrency=4 -B)
5. Enjoy!

Reachable urls:
127.0.0.1:8000/admin - django admin page
127.0.0.1:8000/accounts/login/ - login page
127.0.0.1:8000/rest-auth/registration/ - registration page
127.0.0.1:8000/calendar/event - all events
127.0.0.1:8000/event/day - events for today
127.0.0.1:8000/event/month - events for month