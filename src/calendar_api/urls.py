"""calendar_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_auth.registration.views import VerifyEmailView, RegisterView
from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView 

urlpatterns = [
    path('admin/', admin.site.urls),
    path('/accounts/login/', LoginView.as_view(template_name='registration/login.html',
                                              redirect_authenticated_user=True), name='account_login'),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('rest-auth/registration/verify-email/',
         VerifyEmailView.as_view(), name='account_email_verification_sent'),
    url(r'^account-confirm-email/(?P<key>[-:\w]+)/$', TemplateView.as_view(), name='account_confirm_email'), #https://django-rest-auth.readthedocs.io/en/latest/faq.html?highlight=account_confirm_email
    path('calendar/', include('calendar_core.urls', namespace='calendar_events')),
    path('calendar/', include('calendar_holydays.urls', namespace='calendar_holydays')),
    url(r'^accounts/', include('allauth.urls'))
]
