from django.apps import AppConfig


class CalendarCoreConfig(AppConfig):
    name = 'calendar_core'
