
import os

from celery import Celery
from calendar_api.settings import BROKER_URL

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'calendar_api.settings')

app = Celery('calendar_core', broker=BROKER_URL)
app.config_from_object('django.conf:settings')
# app.conf.update(result_expires=3600, enable_utc=True, timezone='UTC')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

