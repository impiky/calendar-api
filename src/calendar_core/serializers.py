from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import serializers
from calendar_core.models import *
from rest_auth.registration.serializers import RegisterSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username'
        )


class EventSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Event
        fields = (
            'user',
            'event_name',
            'startdatetime',
            'enddatetime',
            'remind_time'
        )


class EventPostSerializer(serializers.ModelSerializer):
    enddatetime = serializers.DateTimeField(required=False)
    remind_time = serializers.ChoiceField(
        choices=REMIND_CHOICES, required=False)

    class Meta:
        model = Event
        fields = (
            'even_name',
            'startdatetime',
            'enddatetime',
            'remind_time'
        )


class CalendarRegisterSerializer(RegisterSerializer):
    country = serializers.CharField(max_length=30, required=False)

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'country': self.validated_data.get('country', '')
        }
