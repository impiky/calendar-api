from datetime import date, datetime, timedelta
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


REMIND_CHOICES = (
    (1, 'one hour'),
    (2, 'two hours'),
    (4, 'four hours'),
    (24, 'day'),
    (168, 'week'),)


class Event(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event_name = models.CharField(verbose_name='Event', max_length=100)
    startdatetime = models.DateTimeField(verbose_name='Start datetime')
    enddatetime = models.DateTimeField(
        verbose_name='End datetime',
        default=datetime.utcnow().combine(date.today(),
                                 datetime.max.time()), blank=True)  # https://stackoverflow.com/questions/2771676/django-datetime-issues-default-datetime-now
    remind_time = models.IntegerField(
        verbose_name='Remind in', choices=REMIND_CHOICES, default=1)

    def __str__(self):
        return self.event_name

    class Meta:
        verbose_name = 'Event'


class UserCountry(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    country = models.CharField(
        verbose_name='Country', max_length=30, blank=True)


@receiver(post_save, sender=Event)
def my_model_post_save(sender, **kwargs):
    from calendar_core.task import send_notification_email
    end_time, remind_time = ('enddatetime', 'remind_time')
    end_time_field = Event._meta.get_field(end_time)
    remind_time_field = Event._meta.get_field(remind_time)
    obj = Event.objects.last()
    end_time_value = end_time_field.value_from_object(obj)
    remind_time_value = remind_time_field.value_from_object(obj)
    send_notification_email.apply_async(
        eta=end_time_value - timedelta(hours=remind_time_value))


@receiver(post_save, sender=User)
def create_user_country(sender, instance, created, **kwargs):
    if created:
        UserCountry.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_country(sender, instance, **kwargs):
    instance.usercountry.save()