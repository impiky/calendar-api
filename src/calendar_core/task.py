from calendar_core.celery import app
from django.contrib.auth import get_user_model
from django.core.mail import send_mail


@app.task
def send_notification_email():
    from .models import Event
    user_value = Event._meta.get_field('user')
    obj = Event.objects.last()
    user_id = user_value.value_from_object(obj)
    UserModel = get_user_model()
    user = UserModel.objects.get(pk=user_id)
    send_mail(
        'test',
        'Hey! Your event starts soon!',
        'djangodjinja@yandex.ru',
        [user.email],
        fail_silently=False,
    )
