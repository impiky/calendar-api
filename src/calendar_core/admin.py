from django.contrib import admin
from calendar_core.models import *

# Register your models here.
admin.site.register(Event)
admin.site.register(UserCountry)