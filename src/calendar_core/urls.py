from django.urls import path
from .views import EventsForDayView, EventsForMonthView, EventsView

app_name = 'calendar_urls'

urlpatterns = [
    path('event/', EventsView.as_view(), name="add_event"),
    path('event/day/', EventsForDayView.as_view()),
    path('event/month/', EventsForMonthView.as_view()),
]
