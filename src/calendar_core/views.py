from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from calendar_core.models import Event
from calendar_core.serializers import EventSerializer, EventPostSerializer
from datetime import datetime


class BaseEventView(APIView):
    @staticmethod
    def get_response(events):
        serializer = EventSerializer(events, many=True)
        return Response({'data': serializer.data})


class EventsForDayView(BaseEventView):
    def get(self, request):
        events = Event.objects.filter(
            user=request.user, startdatetime__year=datetime.today().year,
            startdatetime__month=datetime.today().month,
            startdatetime__day=datetime.today().day)
        return self.get_response(events)


class EventsForMonthView(BaseEventView):
    def get(self, request):
        events = Event.objects.filter(
            user=request.user, startdatetime__year=datetime.today().year,
            startdatetime__month=datetime.today().month)
        return self.get_response(events)


class EventsView(BaseEventView):
    def get(self, request):
        events = Event.objects.filter(user=request.user)
        return self.get_response(events)

    def post(self, request):
        event = EventPostSerializer(data=request.data)
        if event.is_valid():
            event.save(user=request.user)
            return Response({'status': "Added"})
        return Response({'status': "Error"})
